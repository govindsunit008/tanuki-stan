This repository holds the data and machine learning models to help
with GitLab issue triaging.

### Data

`data/` may contain encrypted data. Files ending with `.7z` are stored in
Git LFS and encrypted with a password in the CI/CD variables.

For example, to extract `data/gitlab-issues.7z`, run:

```sh
7z x data/gitlab-issues.7z
```

This data is manually generated from the Rails console. See [this
issue](https://gitlab.com/gitlab-org/gitlab/-/issues/294504) on how to
generate the CSV.

### Training

The `.gitlab-ci.yml` automates the training of this model by:

1. Extracting the encrypted data into the `data/` directory.
1. Converting the Jupyter notebook in `notebooks/classify_groups.ipynb` to a Python script.
1. Running the script, which outputs the trained model and tokenizer state in the `models/` directory.

Training this model requires a GPU with at least 26 GB of RAM. The
following GitLab Runner config illustrates how you might spin up a
custom Google N1 machine to do this:

#### Docker Machine config

```toml
[[runners]]
  name = "test-ml-runner-manager"
  url = "https://gitlab.com/"
  token = "REDACTED"
  executor = "docker+machine"
  [runners.docker]
    tls_verify = false
    image = "ruby:2.7"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    gpus = "all"
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
  [runners.machine]
    IdleCount = 0
    IdleTime = 600
    MaxBuilds = 2
    MachineDriver = "google"
    MachineName = "stanhu-ci-ml-%s"
    MachineOptions = ["google-project=gitlab-internal-153318",
      "google-disk-size=50",
      "google-disk-type=pd-ssd",
      "google-machine-type=n1-custom-4-26624",
      "google-accelerator=count=1,type=nvidia-tesla-p4",
      "google-maintenance-policy=TERMINATE",
      "google-machine-image=https://www.googleapis.com/compute/v1/projects/deeplearning-platform-release/global/images/family/tf2-ent-2-3-cu110", 
      "google-metadata=install-nvidia-driver=True"
    ]
    OffPeakTimezone = ""
    OffPeakIdleCount = 0
    OffPeakIdleTime = 0
```

## References

* YouTube: [TanukiStan: Using Machine Learning for GitLab Issue Triaging](https://www.youtube.com/watch?v=Q4ShY5DsIBY)
* Google Slides Presentation: [Using ML for GitLab Issue Triaging](https://docs.google.com/presentation/d/16Y6GTOY_AffD8vEbE4jkbQw_0s1hC5RbRM-PbuKBAWk/edit?usp=sharing)
